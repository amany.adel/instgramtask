import {combineReducers} from 'redux';
import Auth from './authRedrucer';
import Posts from './PostsReducer';
import BucketList from './bucketListReducer';

export default combineReducers({
    auth : Auth,
    posts : Posts,
    bucketlist: BucketList


});