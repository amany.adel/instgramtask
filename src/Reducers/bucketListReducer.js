
import {
    BUCKETLIST_CHANGED,
    GET_BUCKETLIST_SUCCESS,
    POST_BUCKETLIST_SUCCESS
} from '../Actions/types'

const INITIAL_STATE = {
    allBucketList: [],
    loadingBucketList:false,
    bucketlistName:'',
    postBucketList:false,


};

export default (state = INITIAL_STATE, action) => {
    console.log("action")
    console.log(action);
    switch (action.type) {
        case BUCKETLIST_CHANGED:
            return {...state,bucketlistName:action.payload };
        case GET_BUCKETLIST_SUCCESS:
            return { ...state, allBucketList: action.payload, loadingBucketList: true};
        case POST_BUCKETLIST_SUCCESS:
            return { ...state, postBucketList: true};    
        default:
            return state;
    }

};    