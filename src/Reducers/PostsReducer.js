
import {
    GET_POSTS_SUCCESS,
    GET_USER_POSTS_SUCCESS
} from '../Actions/types'

const INITIAL_STATE = {
    allPosts: [],
    loadingAllPosts:false,
    userPosts:[],
    loadingUserPosts:false

};

export default (state = INITIAL_STATE, action) => {
    console.log("action")
    console.log(action);
    switch (action.type) {
        case GET_POSTS_SUCCESS:
            console.log('all posts redux')
            console.log(action.payload[0])
            return { ...state, allPosts: action.payload, loadingAllPosts: true };
        case GET_USER_POSTS_SUCCESS:
            return { ...state, userPosts: action.payload, loadingUserPosts: true};
        default:
            return state;
    }

};    