import {
     USERNAME_CHANGED ,
     PASSWORD_CHANGED ,
     LOGIN_USER_SUCCESS,
     LOGIN_USER_FAILED,
    } from '../Actions/types'

const INITIAL_STATE={
    username:'',
    user:null,
    password:'',
    errMsg:null,
    loading:null,

};

export default (state = INITIAL_STATE , action)=>{
    console.log("action")
    console.log(action);
    switch(action.type)
    {
        case USERNAME_CHANGED:
            return {...state,username:action.payload ,loading:null,errMsg:null };
        case PASSWORD_CHANGED:
            return {...state ,password:action.payload ,loading:null ,errMsg:null};
        case LOGIN_USER_SUCCESS:
            console.log('success');
            console.log(action.payload)
            return {...state, ...INITIAL_STATE ,user:action.payload,loading:true};
        case LOGIN_USER_FAILED:
            return {...state , ...INITIAL_STATE , errMsg:"Authentication Failed !" , loading:false};   
        
        default:
            return state;
    }

};