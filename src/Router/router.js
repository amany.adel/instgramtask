import React from 'react';
import {
  Login,
  Home,
  Profile,
  AddPost,
  BucketList
} from '../Screens';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';

const Tab = createBottomTabNavigator();

const Stack = createStackNavigator();
const HomeStack = createStackNavigator();

function Screens() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          if (route.name === 'NewsFeed') {
            iconName = focused
              ? 'home'
              : 'home';
          } else if (route.name === 'Profile') {
            iconName = focused
              ? 'user-circle'
              : 'user-circle';
          }
          else if (route.name === 'BucketList') {
            iconName = focused
              ? 'inbox'
              : 'inbox';
          }

          return <Icon name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: '#76D0D1',
        inactiveTintColor: 'gray',
      }}>
      <Tab.Screen name="NewsFeed" >
        {() => (
          <HomeStack.Navigator  screenOptions={{
            headerShown: false
          }}>
            <HomeStack.Screen
              name="Home"
              component={Home}
            />
            <HomeStack.Screen
              name="AddPost"
              component={AddPost}
            />
          </HomeStack.Navigator>
        )}
      </Tab.Screen>

      <Tab.Screen name="BucketList" component={BucketList} />

      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
}
const RouterComponent = () => {

  return (

    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login"
       screenOptions={{
        headerShown: false
      }}
      >

        <Stack.Screen
          name="login"
          component={Login}
          // options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Home"
          component={Screens}
          // options={{
          //   title: 'Add Post',
          //   headerLeft: null,
          //   headerStyle: {
          //     backgroundColor: 'white'
          //   },
          //   headerTintColor: 'black',
          //   headerTitleStyle: {
          //     // fontWeight: 'bold',
          //     alignSelf: 'center'
          //   }
          // }}

        />

      </Stack.Navigator>
    </NavigationContainer>
  );
}


export default RouterComponent;