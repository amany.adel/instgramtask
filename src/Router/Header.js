
import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, Picker, AsyncStorage, Platform } from 'react-native';

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
    responsiveScreenHeight
} from "react-native-responsive-dimensions";

const Header = ({ name }) => (
    <View style={{
        width: "100%",
        height: responsiveHeight(8),
        paddingTop: responsiveHeight(2),
        backgroundColor: 'white',
    }}>
        <Text allowFontScaling={false} style={{ color: '#F8AAA8', textAlign: 'center', fontWeight: 'bold', fontSize: responsiveFontSize(2.5) }}>{name}</Text>


    </View>
);


const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        paddingTop: 40,
        alignItems: "center",
        flex: 1

    },
    listItem: {
        height: 60,
        alignItems: "center",
        flexDirection: "row",
    },
    title: {
        fontSize: 18,
        marginLeft: 20
    },
    header: {
        width: "100%",
        height: 60,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: 20
    },
    profileImg: {
        width: 80,
        height: 80,
        borderRadius: 40,
        marginTop: 20
    },
    sidebarDivider: {
        height: 1,
        width: "100%",
        backgroundColor: "lightgray",
        marginVertical: 10
    },
    buttonLang: {
        // color: '#428dff', fontWeight: 'bold', fontSize: 23
    },
    buttonLangText: {
        color: '#428dff', fontWeight: 'bold', fontSize: 18, marginRight: 15
    }
});

export default Header;  