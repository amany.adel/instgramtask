import {
    USERNAME_CHANGED,
    PASSWORD_CHANGED,
    LOGIN_USER_FAILED,
    LOGIN_USER_SUCCESS,
    GET_POSTS_SUCCESS,
    GET_USER_POSTS_SUCCESS,
    BUCKETLIST_CHANGED,
    GET_BUCKETLIST_SUCCESS,
    POST_BUCKETLIST_SUCCESS
  } from './types';
import Api from '../Api';
import {Alert} from 'react-native';

  
  export const usernameChanged = text => {
    return {
      type: USERNAME_CHANGED,
      payload: text,
    };
  };
    
  export const bucketlistChanged = text => {
    return {
      type: BUCKETLIST_CHANGED,
      payload: text,
    };
  };
  
  export const passwordChanged = text => {
    return {
      type: PASSWORD_CHANGED,
      payload: text,
    };
  };
  
  export const loginUser = (username,password) => {
    return dispatch => {
      console.log('in action index')
      Api.LoginUser(username,password)
        .then(res => {
          console.log('1')
          console.log(res[0].status)
          if (res[0].status =="Login successfully")
            dispatch({type: LOGIN_USER_SUCCESS, payload: res[0]});
          else dispatch({type: LOGIN_USER_FAILED, payload: res});
        })
        .catch(() => {
          Alert.alert('Warning', 'Check your connection!');
        });
    };
  }; 

  export const userPosts = (userId) => {
    return dispatch => {
      console.log('in action index')
      Api.UserPosts(userId)
        .then(res => {
          console.log(res)
          // if (res[0].status =="Login successfully")
            dispatch({type: GET_USER_POSTS_SUCCESS, payload: res});
        })
        
        .catch(() => {
          Alert.alert('Warning', 'Check your connection!');
        });
    };
  };



  export const allPosts = () => {
    return dispatch => {
      console.log('in action index')
      Api.AllPosts()
        .then(res => {
          console.log(res)
          // if (res[0].status =="Login successfully")
            dispatch({type: GET_POSTS_SUCCESS, payload: res});
        // }
        })
        .catch(() => {
          Alert.alert('Warning', 'Check your connection!');
        });
    };
  }; 


  export const userBucketlist = (userId) => {
    return dispatch => {
      console.log('in action index')
      Api.AllBuckets(userId)
        .then(res => {
          console.log(res)
          // if (res[0].status =="Login successfully")
            dispatch({type: GET_BUCKETLIST_SUCCESS, payload: res});
        })
        
        .catch(() => {
          Alert.alert('Warning', 'Check your connection!');
        });
    };
  };

  