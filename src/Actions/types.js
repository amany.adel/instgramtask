export const USERNAME_CHANGED='username_changed';

export const BUCKETLIST_CHANGED='bucketlist_changed';

export const PASSWORD_CHANGED='password_changed';

export const LOGIN_USER_SUCCESS='login_user_success';

export const LOGIN_USER_FAILED='login_user_failed';

export const GET_POSTS_SUCCESS='get_posts_success';

export const GET_USER_POSTS_SUCCESS='get_user_posts_success';

export const GET_BUCKETLIST_SUCCESS='get_bucketlist_success';

export const POST_BUCKETLIST_SUCCESS='post_bucketlist_success';
