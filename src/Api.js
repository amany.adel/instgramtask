import axios from 'axios';


class Api {
    static url = 'http://192.168.1.110:4000/';
    static init = data => {
        return {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: data,
        };
    };

    static initGet(version) {
        const init = {
            // return {

            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
            body: version,
            // }
        };
    }
    static LoginUser(username, password) {
        console.log('api');
        return axios
            (
                `${this.url}users?username=${username}&password=${password}`
                , this.initGet()
            )
            .then(function (response) {
                // handle success
                // alert(JSON.stringify(response.data));
                return response.data;
            })
            .catch(function (error) {
                // handle error
                // console.log(error)
                alert(error.message);
            })
    }

    // ********************************************************* images in user profile
    static UserPosts(userId) {
        console.log('api');
        return axios
            (
                `${this.url}posts?userInfo.userId=${userId}`
                , this.initGet()
            )
            .then(function (response) {
                return response.data;
            })
            .catch(function (error) {
                alert(error.message);
            })

    }

    // ****************************** all posts in home screen
    static AllPosts() {
        console.log('api');
        return axios
            (
                `${this.url}posts`
                // "http://192.168.1.110:4000/posts"
                , this.initGet()
            )
            .then(function (response) {
                console.log('api ..... all posts')
                console.log(response.data[0].from)
                return response.data;
            })
            .catch(function (error) {
                alert(error.message);
            })

    }

    // ****************************** post bucketlist
    static AddBucketList(userId, name) {

        console.log('api');

        return axios.post
            (
                `${this.url}/bucketList`,
                {
                    "userId": userId,
                    "name": name
                },
                {'Content-Type':'application/json;charset=UTF-8'}
                //    ,{ validateStatus: false },



            )
            .then(function (response) {
                console.log('api ..... add bucketlist')
                console.log(response.data)
                // return response.data;
            })
            .catch(function (error) {
                // // alert(error.message);
                // console.log(error.message)
                if (error.response) {
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                }
            })


    }
    // ****************************** all bucketlist in bucketlist screen
    static AllBuckets(userId) {
        console.log('api');
        return axios
            (
                `${this.url}bucketList/?userId=${userId}`
                , this.initGet()
            )
            .then(function (response) {

                return response.data;
            })
            .catch(function (error) {
                alert(error.message);
            })

    }

}
export default Api;
