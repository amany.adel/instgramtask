import React from 'react';
import { View } from 'react-native';

const InputView = (props) => {
    return (
        <View style={[styles.containerStyle, props.style]}>

        {props.children}

        </View>
    );
};

const styles = {
    containerStyle: {
        marginHorizontal: '10%',
        marginBottom: '5%'
    }    
};

export { InputView };