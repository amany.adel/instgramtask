
import { View, Text, ImageBackground, ScrollView,StyleSheet, TouchableOpacity ,FlatList} from "react-native";
import React, { Component } from "react";
import Header from '../Router/Header';
import { connect } from 'react-redux';
import {InputView , MainSquaresHome, InputText} from '../Components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {bucketlistChanged,userBucketlist} from '../Actions';
import LinearGradient from 'react-native-linear-gradient';

import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
    responsiveScreenHeight
} from "react-native-responsive-dimensions";
import { Input ,Button,ListItem} from 'react-native-elements';
import Api from "../Api";


class BucketList extends Component {
    constructor(props) {
        super(props);
        this.state={
            allBucketlistScreen:this.props.allBucketlistInScreen,
        }
    }



    componentDidMount() {
        this.props.userBucketlist(this.props.user.id)
      
    }
    componentWillReceiveProps(nextProps) {
        console.log('in willl recieve props');
    
        if (nextProps.allBucketlistInScreen !== this.state.allBucketlistScreen) {
          console.log('inside If willl recieve props');
    
          this.setState({
            allBucketlistScreen: nextProps.allBucketlistInScreen,
          });
        }
      }
    onBucketlistChange(text) {
        this.props.bucketlistChanged(text);
    }

    addBucketlist()
    {
        Api.AddBucketList({"userId":1,
        "name":"amany"})
    }

    renderItem = ({ item }) => {
        return (
            <ListItem bottomDivider>
            <ListItem.Content>
              <ListItem.Subtitle style={{
                  fontSize:responsiveFontSize(2.3),
                  fontWeight:'bold',
                  color:'#FAB0AD'
              }}>{item.name}</ListItem.Subtitle>
            </ListItem.Content>
            {/* <ListItem.Chevron /> */}
          </ListItem>

        )
    };
    render() {
        if(this.state.allBucketlistScreen != 0  && this.props.loadingBucketListInScreen)
        {
        return (
            <View>
            <Header
                name='Bucket List'
            />
            <View style={{
                flexDirection: 'row',
                marginHorizontal:responsiveWidth(2)
            }}>
             <Input
                placeholder="Add your bucket list .."
                value={this.props.bucketlistName}
                onChangeText={this.onBucketlistChange.bind(this)} 
                containerStyle={{
                    width:responsiveWidth(80),
                    marginTop:responsiveHeight(2),
                    color:'#FCC0C0'


                }}
                />
               <Button
                title="Add"
                containerStyle={{
                    marginTop:responsiveHeight(3),
                }}
                buttonStyle={{
                    backgroundColor:'#FAB0AD',
                    width:responsiveWidth(15)
                }}
                onPress={
                    this.addBucketlist.bind(this)
                }
                />

            </View>
            <FlatList
                data={this.state.allBucketlistScreen}
                renderItem={ this.renderItem}
                keyExtractor={(item) => item.id}

            />
          

            </View>
        );}
        else
        {
            return (
                <ScrollView style={{backgroundColor:'white',height:responsiveHeight(100)}}>
                <Header
                    name='Home'
                />
                <View style={{marginTop:responsiveHeight(20)}}>
                    <Text style={{
                        color:'red',
                        textAlign:'center',
                        fontSize:responsiveFontSize(3.5),
                        // fontWeight:'bold'
                    }}
                    >Oops...</Text>
                    <Text style={{
                        fontSize:responsiveFontSize(2.2),
                        marginTop:responsiveHeight(1),
                        textAlign:'center',
    
                    }}
                    >We couldn`t find Bucketlist at this time</Text>
                </View>           
                <View style={styles.btnView}>
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#f78484','#f78787','#fab7b7']} style={styles.loginBtn} >
    
                        <TouchableOpacity
                            onPress={()=>{
                                this.props.allPosts()
    
                            }}
                        >
                            <Text allowFontScaling={false} style={styles.loginBtnText}>Try Again</Text>
                        </TouchableOpacity>
                    </LinearGradient>
    
                </View>
                </ScrollView>
            );
        }    
    }
}

const styles = StyleSheet.create({
    bucketListInputView: {
        width: '50%'
    },
    input: {
        borderWidth: 1.5,
        borderColor: '#FAB0AD',
        height:responsiveHeight(10),
        width:responsiveWidth(40),
        marginTop: responsiveHeight(3),
        // paddingVertical: 5,
        color: '#83CDCE', fontSize: 25
    },
    thirdTextSize: {
        width: '20%'
    },
    thirdText: {
        fontSize: 18,
        color: '#AFB2B9',
        marginRight: responsiveWidth(5),
    },
    btnView: {
        alignItems: 'center',
        marginVertical: '5%'
    },
    loginBtn: {
        padding: 8,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        // backgroundColor: "#222"
        borderRadius: 10,
        width: '40%',
    },
    loginBtnText: {
        fontWeight: 'bold',
        color: 'white',
        fontSize: 18,
        paddingVertical: '3%',
        paddingHorizontal: '15%',
    }

});

const mapStateToProps = state => {
    return {
        user: state.auth.user,
        bucketlistName:state.bucketlist.bucketlistName,
        allBucketlistInScreen:state.bucketlist.allBucketList,
        loadingBucketListInScreen:state.bucketlist.loadingBucketList





    };

};

const BucketListRedux = connect(mapStateToProps, {
    bucketlistChanged,
    userBucketlist

})(BucketList);
export { BucketListRedux as BucketList };