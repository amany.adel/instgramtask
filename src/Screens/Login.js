
import { View, Text, ImageBackground, TextInput, StyleSheet, TouchableOpacity } from "react-native";
import React, { Component } from "react";
import { connect } from 'react-redux';
import {
    loginUser,
    usernameChanged,
    passwordChanged,
} from '../Actions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from 'react-native-linear-gradient';
import { InputView, Label, InputText } from '../Components'
import Api from '../Api'
class Login extends Component {

    constructor(props) {
        super(props);
        this.state={
            loginLoading:false,
            errMsg:null
        }
       
    }



    componentDidMount() {

    }



    onUsernameChange(text) {
        this.props.usernameChanged(text);
    }

    onPasswordChange(text) {
        this.props.passwordChanged(text);
    }

    onButtonPressLogin() {
        const { username, password } = this.props;


        this.props.loginUser(username,password);
        // if(this.props.loading)
        //      this.props.navigation.navigate('Home');



    }

    loginState() {
        console.log('loading')
        console.log(this.state.loginLoading)
        if (this.props.loading) {
            this.props.navigation.navigate('Home');

        }
        if (this.props.errMsg)
            Alert.alert("Warning", this.props.errMsg);
    }
    render() {
        return (
            <View>
                <ImageBackground style={styles.imageBackgroundStyle}
                    source={require('../Assets/img/loginCover5.jpg')}
                >
                    <View style={styles.imageBackgroundView}>
                        <View style={{
                            marginTop: '30%'
                        }}>
                            <Text style={styles.iconLogo}>
                                <Icon name="camera-retro" size={80} color="white" />
                            </Text>
                            <View >

                                <InputView>
                                    <Label style={styles.label}>Username</Label>
                                    <InputText style={styles.input} secure={false} 
                                     value={this.props.username}
                                     onChangeText={this.onUsernameChange.bind(this)} 
                                    />
                                </InputView>

                                <InputView>

                                    <Label style={styles.label}>Password</Label>
                                    <InputText style={styles.input} secure={true} 
                                    value={this.props.password}
                                    onChangeText={this.onPasswordChange.bind(this)} 
                                    />

                                </InputView>

                                <View style={styles.btnView}>
                                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#FAB0AD', '#FCB0B2', '#FCC0C0']} style={styles.loginBtn} >

                                        <TouchableOpacity
                                            onPress={this.onButtonPressLogin.bind(this)}
                                        >
                                            <Text allowFontScaling={false} style={styles.loginBtnText}>Login</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>

                                </View>
                            </View>
                        </View>

                    </View>
                </ImageBackground>
                {this.loginState()}

            </View>

        );
    }



}

const styles = StyleSheet.create({
    imageBackgroundStyle: {
        width: '100%',
        height: '100%'
    },
    imageBackgroundView: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    iconLogo: {
        textAlign: 'center',
        marginBottom: '15%'
    },
    // inputView:{
    //     marginHorizontal: '10%',
    //     marginBottom: '5%'
    // },
    label: {
        color: 'white',
        fontSize: 18
    },
    input: {
        borderBottomWidth: 1.5,
        borderColor: 'white',
        // height:30,
        paddingTop: 0,
        paddingVertical: 5,
        color: '#83CDCE', fontSize: 18
    },
    btnView: {
        alignItems: 'center',
        marginVertical: '5%'
    },
    loginBtn: {
        padding: 8,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        // backgroundColor: "#222"
        borderRadius: 30,
        width: '40%',
    },
    loginBtnText: {
        fontWeight: 'bold',
        color: 'white',
        fontSize: 18,
        paddingVertical: '3%',
        paddingHorizontal: '15%',
    }
});

const mapStateToProps = state => {
    return {
        username: state.auth.username,
        password: state.auth.password,
        errMsg: state.auth.errMsg,
        loading: state.auth.loading,
        language: state.translation,
        user: state.auth.user


    };

};


const LoginRedux = connect(mapStateToProps, {
    usernameChanged,
    passwordChanged,
    loginUser

})(Login);
export { LoginRedux as Login };