
import { View, Text, BackHandler, ScrollView, FlatList,StyleSheet, Alert,TouchableOpacity } from "react-native";
import React, { Component } from "react";
import Header from '../Router/Header';
import { connect } from 'react-redux';
import {
    allPosts
} from '../Actions';
import { MainSquaresHome, ImageHome } from '../Components';
import {
    responsiveHeight,
    responsiveWidth,
    responsiveFontSize,
    responsiveScreenHeight
} from "react-native-responsive-dimensions";
import LinearGradient from 'react-native-linear-gradient';

console.disableYellowBox = true;

class Home extends Component {
    constructor(props) {
        super(props);
        this.state={
            allPostsHome:this.props.allPostsInHome,
        }
    }



    componentDidMount() {
        console.log("********************** 12")
        console.log(this.state.allPostsHome)

        this.props.allPosts();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

    }
 
    componentWillReceiveProps(nextProps) {
        console.log('in willl recieve props');
    
        if (nextProps.allPostsInHome !== this.state.allPostsHome) {
          console.log('inside If willl recieve props');
    
          this.setState({
            allPostsHome: nextProps.allPostsInHome,
          });
        }
      }
    // componentWillUnmount()
    // {
    //     this.props.allPostsInHome;
    // }  
    onButtonPress = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        // then navigate
        navigate('NewScreen');
    }
    handleBackButton = () => {
        Alert.alert(
            'Exit App',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            }, ], {
                cancelable: false
            }
         )
         return true;
       } 

    
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
   

    onButtonPressPostBtn() {
        this.props.navigation.navigate('AddPost');

    }

    renderItem = ({ item }) => {
        return (
            <MainSquaresHome>
                <View style={styles.firstImageSize}>
                    <ImageHome source={{uri:item.userInfo.userImg}} style={styles.profileImage} />
                    <ImageHome source={{uri:item.imgPost}} style={styles.postImage} />
                    <Text style={ styles.likeText}> {item.numOfLikes} like</Text>
                </View>
                <View
                    style={styles.secondTextSize}
                >
                    <Text style={styles.secondText}>{item.userInfo.userFullName}</Text>

                </View>
                <View
                    style={styles.thirdTextSize}
                >
                    <Text style={styles.thirdText}>{item.from}d</Text>

                </View>
            </MainSquaresHome>

        )
    };

    render() {
        if(this.state.allPostsHome != 0  && this.props.loadingAllPosts)
        {
            return (
                <ScrollView style={{
                    flexDirection: 'column',
                    flex: 1,
                    backgroundColor: 'white'
                }}>
                    <Header
                        name='Home'
                    />
                    {/* <View> */}
                       
                    <FlatList
                        data={this.state.allPostsHome}
                        renderItem={ this.renderItem}
                        keyExtractor={(item) => item.id}

                        // numColumns={3}
                    />

                </ScrollView>
            );
        }
        else
        {
            return (
                <ScrollView style={{backgroundColor:'white',height:responsiveHeight(100)}}>
                <Header
                    name='Home'
                />
                <View style={{marginTop:responsiveHeight(20)}}>
                    <Text style={{
                        color:'red',
                        textAlign:'center',
                        fontSize:responsiveFontSize(3.5),
                        // fontWeight:'bold'
                    }}
                    >Oops...</Text>
                    <Text style={{
                        fontSize:responsiveFontSize(2.2),
                        marginTop:responsiveHeight(1),
                        textAlign:'center',
    
                    }}
                    >We couldn`t find posts at this time</Text>
                </View>           
                <View style={styles.btnView}>
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#f78484','#f78787','#fab7b7']} style={styles.loginBtn} >
    
                        <TouchableOpacity
                            onPress={()=>{
                                this.props.allPosts()
    
                            }}
                        >
                            <Text allowFontScaling={false} style={styles.loginBtnText}>Try Again</Text>
                        </TouchableOpacity>
                    </LinearGradient>
    
                </View>
                </ScrollView>
            );
        }    
    }
}

const styles = StyleSheet.create({
    profileImage: {
        width: responsiveWidth(8),
        height: responsiveHeight(5),
        borderRadius: 100 / 2,
        marginLeft: responsiveWidth(5),

    },
    postImage: {
        width: responsiveWidth(100),
        height: responsiveHeight(35),
        marginVertical: responsiveHeight(1)
    },
    firstImageSize: {
        width: '15%'
    },
    secondTextSize: {
        width: '65%'
    },
    secondText: {
        fontSize: 18,
        color: 'black',
        paddingLeft: responsiveWidth(2)
    },
    thirdTextSize: {
        width: '20%'
    },
    thirdText: {
        fontSize: 18,
        color: '#AFB2B9',
        marginRight: responsiveWidth(5),
    },
    likeText:{
        color: '#AFB2B9',
        marginLeft: responsiveWidth(2) 
    },
    btnView: {
        alignItems: 'center',
        marginVertical: '5%'
    },
    loginBtn: {
        padding: 8,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        // backgroundColor: "#222"
        borderRadius: 10,
        width: '40%',
    },
    loginBtnText: {
        fontWeight: 'bold',
        color: 'white',
        fontSize: 18,
        paddingVertical: '3%',
        paddingHorizontal: '15%',
    }
});


const mapStateToProps = state => {
    return {
        user: state.auth.user,
        allPostsInHome:state.posts.allPosts,
        loadingAllPosts:state.posts.loadingAllPosts,

    };

};


const HomeRedux = connect(mapStateToProps, {
    allPosts

})(Home);
export { HomeRedux as Home };
