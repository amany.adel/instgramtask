
import { View, Text, TouchableOpacity, Image,StyleSheet, FlatList, ScrollView } from "react-native";
import React, { Component } from "react";
import { connect } from 'react-redux';
import { ImageHome } from '../Components/ImageHome';
import {
    userPosts
} from '../Actions';
import Header from '../Router/Header';
import { responsiveFontSize, responsiveHeight, responsiveScreenFontSize, responsiveWidth } from "react-native-responsive-dimensions";
import LinearGradient from 'react-native-linear-gradient';

class Profile extends Component {
    constructor(props){
        super(props)
        this.state = {
            userPosts:[],
            GridListItems: [
              { key:'https://cdn1.medicalnewstoday.com/content/images/articles/322/322868/golden-retriever-puppy.jpg'  },
            ]};

            // this.props.userPosts(this.props.user.id)
    }

    componentDidMount() {
        console.log("********************** 1")
        this.props.userPosts(this.props.user.id)
    }
    componentWillReceiveProps(nextProps) {
        console.log('in willl recieve props');
    
        if (nextProps.allUserPosts !== this.state.userPosts) {
          console.log('inside If willl recieve props');
    
          this.setState({
            userPosts: nextProps.allUserPosts,
          });
        }
      }
    render() {
        if(this.state.userPosts.length != 0 && this.props.loadingUserPosts)
        {
            return (
                <ScrollView style={{height:responsiveHeight(100)}}>
                <Header
                    name='Profile'
                />
                  <View style={styles.profileView}>
                    <Image source={{uri:this.props.user.userImg}} style={styles.profileImage}/>
                    <View style={{
                        marginVertical:responsiveHeight(1.5)
                    }}>
                        <Text style={styles.mainText}>{this.props.user.full_name} </Text>
                        <Text style={styles.secondText}>{this.props.user.email} </Text>
                    </View>
                    <Text style={styles.secondText}>{this.props.user.age} years old</Text>
    
                  </View>
                  <View style={styles.container}>
                    <FlatList
                        data={ this.state.userPosts.length != 0? this.state.userPosts:[] }
                        renderItem={ ({item}) =>
                        <View style={styles.GridViewContainer}>
                        <ImageHome source={{uri:item.imgPost}} style={styles.GridViewImageLayout} />
    
                        </View> }
                        numColumns={3}
                    />
                </View>
    
                </ScrollView>
            );
        }
        else
        return (
            <ScrollView style={{backgroundColor:'white',height:responsiveHeight(100)}}>
            <Header
                name='Profile'
            />
            <View style={{marginTop:responsiveHeight(20)}}>
                <Text style={{
                    color:'red',
                    textAlign:'center',
                    fontSize:responsiveFontSize(3.5),
                    // fontWeight:'bold'
                }}
                >Oops...</Text>
                <Text style={{
                    fontSize:responsiveFontSize(2.2),
                    marginTop:responsiveHeight(1),
                    textAlign:'center',

                }}
                >We couldn`t find your posts at this time</Text>
            </View>           
            <View style={styles.btnView}>
                <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#f78484','#f78787','#fab7b7']} style={styles.loginBtn} >

                    <TouchableOpacity
                        onPress={()=>{
                            this.props.userPosts(this.props.user.id)

                        }}
                    >
                        <Text allowFontScaling={false} style={styles.loginBtnText}>Try Again</Text>
                    </TouchableOpacity>
                </LinearGradient>

            </View>
            </ScrollView>
        );
       
    }
}

const styles = StyleSheet.create({
    profileView:
    {
        backgroundColor:'white',
        borderTopWidth:1,
        borderColor:'#D5D8DB',
        borderBottomWidth:1,
        paddingVertical:responsiveHeight(3),
        paddingHorizontal:responsiveWidth(8)
    },
    profileImage:{
        width:responsiveWidth(25),
        height:responsiveHeight(16)
    },
    mainText:{
        color:'black',
        fontWeight:'bold',
        fontSize:18
    },
    secondText:{
        color:'#ABAFB5',
        fontSize:16

    },  
    container: {
        flex: 1,
        justifyContent: "center",
        // backgroundColor: "#e5e5e5"
    },
    headerText: {
        fontSize: 20,
        textAlign: "center",
        margin: 10,
        fontWeight: "bold"
    },
    GridViewContainer: {
        flex:1,
        height:responsiveHeight(20),
        width:responsiveWidth(30),
        margin: 5,
    },
    GridViewImageLayout: {
        height:responsiveHeight(20),
        width:responsiveWidth(32)
    },
    btnView: {
        alignItems: 'center',
        marginVertical: '5%'
    },
    loginBtn: {
        padding: 8,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        // backgroundColor: "#222"
        borderRadius: 10,
        width: '40%',
    },
    loginBtnText: {
        fontWeight: 'bold',
        color: 'white',
        fontSize: 18,
        paddingVertical: '3%',
        paddingHorizontal: '15%',
    }
    
    
    
});


const mapStateToProps = state => {
    return {
        user: state.auth.user,
        allUserPosts:state.posts.userPosts,
        loadingUserPosts:state.posts.loadingUserPosts




    };

};


const ProfileRedux = connect(mapStateToProps, {
    userPosts

})(Profile);
export { ProfileRedux as Profile };
